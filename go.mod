module st1v2

go 1.12

require (
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/go-chi/chi v4.0.2+incompatible
	github.com/go-chi/cors v1.0.0
	github.com/jinzhu/gorm v1.9.8
	github.com/lib/pq v1.1.1
	golang.org/x/crypto v0.0.0-20190513172903-22d7a77e9e5f
)
