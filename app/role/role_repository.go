package role

import (
	"github.com/jinzhu/gorm"
	"st1v2/models"
)

type IRole interface {
	GetById(id int64) (models.Role, error)
	GetAll() ([]models.Role, error)
}

type RoleRepo struct {
	DB *gorm.DB
}

func NewRepository(db *gorm.DB) *RoleRepo {
	return &RoleRepo{db}
}

func (r *RoleRepo) GetById(id int64) (models.Role, error) {
	role := models.Role{}
	err := r.DB.Find(&role, id).Error
	if err != nil {
		return models.Role{}, err
	}
	return role, nil
}
func (r *RoleRepo) GetAll() ([]models.Role, error) {
	listRole := []models.Role{}
	err := r.DB.Not([]int64{-1, 0, 1}).Find(&listRole).Error
	if err != nil {
		return nil, err
	}
	return listRole, nil
}
