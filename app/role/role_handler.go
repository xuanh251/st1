package role

import (
	"net/http"
	"st1v2/configs/database"
	"st1v2/utils"
)

type Handler struct {
	service IRoleService
}

func NewHTTPHandler(db *database.SQL) *Handler {
	r := NewRepository(db.DB)
	u := NewService(db.DB, r)
	return &Handler{u}
}
func (h *Handler) GetAll(w http.ResponseWriter, r *http.Request) {
	rs, err := h.service.GetAll()
	if err != nil {
		utils.RespondWithError(w, http.StatusUnprocessableEntity, "Data error"+err.Error())
		return
	}
	utils.RespondWithJSON(w, http.StatusOK, rs)
}
