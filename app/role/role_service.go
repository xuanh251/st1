package role

import (
	"github.com/jinzhu/gorm"
)

type RoleService struct {
	db         *gorm.DB
	repository IRole
}

func NewService(db *gorm.DB, r IRole) *RoleService {
	return &RoleService{db, r}
}

type IRoleService interface {
	GetAll() (interface{}, error)
}

func (s *RoleService) GetAll() (interface{}, error) {
	role, err := s.repository.GetAll()
	if err != nil {
		return nil, err
	}
	return role, nil
}
