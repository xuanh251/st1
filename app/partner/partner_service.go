package partner

import (
	"errors"
	"github.com/jinzhu/gorm"
	"st1v2/models"
	"st1v2/utils"
)

type IPartnerService interface {
	GetAll(opt []string) (interface{}, error)
	GetById(id int64) (interface{}, error)
	Create(user models.Partner) error
	Update(id int64, updateInfo map[string]interface{}) error
	Delete(id int64) error
}
type PartnerService struct {
	db         *gorm.DB
	repository IPartner
}

func NewService(db *gorm.DB, r IPartner) *PartnerService {
	return &PartnerService{db, r}
}

func (s *PartnerService) GetAll(opt []string) (interface{}, error) {
	var rs []interface{}
	pc, err := s.repository.GetAll()
	if err != nil {
		return nil, err
	}
	if opt[0] != "*" {
		for _, elem := range pc {
			m := utils.CustomFeild(elem, opt)
			if len(m) == 0 {
				return nil, errors.New("Bad request payload")
			}
			rs = append(rs, m)
		}
		return rs, nil
	}
	return pc, nil
}

func (s *PartnerService) GetById(id int64) (interface{}, error) {
	return s.repository.GetById(id)
}

func (s *PartnerService) Create(data models.Partner) error {
	return s.repository.Create(data)
}

func (s *PartnerService) Update(id int64, updateInfo map[string]interface{}) error {
	return s.repository.Update(id, updateInfo)
}

func (s *PartnerService) Delete(id int64) error {
	return s.repository.Delete(id)
}
