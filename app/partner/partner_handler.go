package partner

import (
	"encoding/json"
	"github.com/go-chi/chi"
	"github.com/lib/pq"
	"io/ioutil"
	"net/http"
	"st1v2/configs/database"
	"st1v2/models"
	"st1v2/utils"
	"strconv"
	"strings"
)

type PartnerHandler struct {
	service IPartnerService
}

func NewHTTPHandler(db *database.SQL) *PartnerHandler {
	r := NewRepository(db.DB)
	u := NewService(db.DB, r)
	return &PartnerHandler{u}
}
func (h *PartnerHandler) GetAll(w http.ResponseWriter, r *http.Request) {
	var requestData map[string]string
	body, _ := ioutil.ReadAll(r.Body)
	err := json.Unmarshal(body, &requestData)
	if err != nil {
		utils.RespondWithError(w, http.StatusBadRequest, err.Error())
		return
	}
	queryData := requestData["query"]
	if queryData == "" {
		utils.RespondWithError(w, http.StatusBadRequest, err.Error())
		return
	}
	requestField := strings.Split(queryData, ", ")
	rs, err := h.service.GetAll(requestField)
	if err != nil {
		utils.RespondWithError(w, http.StatusUnprocessableEntity, err.Error())
		return
	}
	utils.RespondWithJSON(w, 200, rs)
}
func (h *PartnerHandler) GetById(w http.ResponseWriter, r *http.Request) {
	id, err := strconv.ParseInt(chi.URLParam(r, "id"), 10, 64)
	if err != nil {
		utils.RespondWithError(w, http.StatusUnprocessableEntity, err.Error())
		return
	}
	pc, err := h.service.GetById(id)
	if err != nil {
		utils.RespondWithError(w, http.StatusUnprocessableEntity, "Data error"+err.Error())
		return
	}
	utils.RespondWithJSON(w, http.StatusOK, pc)
}
func (h *PartnerHandler) Create(w http.ResponseWriter, r *http.Request) {
	obj := models.Partner{}
	body, _ := ioutil.ReadAll(r.Body)
	err := json.Unmarshal(body, &obj)
	if err != nil {
		utils.RespondWithError(w, http.StatusBadRequest, err.Error())
		return
	}
	err = h.service.Create(obj)
	if err != nil {
		utils.RespondWithError(w, http.StatusUnprocessableEntity, err.(*pq.Error).Detail)
		return
	}
	utils.RespondWithJSON(w, 201, "Create successfully!")
}
func (h *PartnerHandler) Update(w http.ResponseWriter, r *http.Request) {
	id, err := strconv.ParseInt(chi.URLParam(r, "id"), 10, 64)
	if err != nil {
		utils.RespondWithError(w, http.StatusUnprocessableEntity, err.Error())
		return
	}
	var updateInfo map[string]interface{}
	body, _ := ioutil.ReadAll(r.Body)
	err = json.Unmarshal(body, &updateInfo)
	if err != nil {
		utils.RespondWithError(w, http.StatusBadRequest, err)
		return
	}
	err = h.service.Update(id, updateInfo)
	if err != nil {
		utils.RespondWithError(w, http.StatusUnprocessableEntity, err.(*pq.Error).Detail)
		return
	}
	utils.RespondWithJSON(w, http.StatusOK, "Cập nhật thành công")
}
func (h *PartnerHandler) Delete(w http.ResponseWriter, r *http.Request) {
	id, err := strconv.ParseInt(chi.URLParam(r, "id"), 10, 64)
	if err != nil {
		utils.RespondWithError(w, http.StatusUnprocessableEntity, err.Error())
		return
	}
	err = h.service.Delete(id)
	if err != nil {
		utils.RespondWithError(w, http.StatusUnprocessableEntity, "Data error"+err.Error())
		return
	}
	utils.RespondWithJSON(w, http.StatusOK, "Xoá thành công!")
}
