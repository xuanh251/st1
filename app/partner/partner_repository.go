package partner

import (
	"github.com/jinzhu/gorm"
	"st1v2/models"
)

type IPartner interface {
	GetAll() ([]models.Partner, error)
	GetById(id int64) (models.Partner, error)
	Create(pc models.Partner) error
	Update(id int64, updateInfo map[string]interface{}) error
	Delete(id int64) error
}
type PartnerRepo struct {
	DB *gorm.DB
}

func NewRepository(db *gorm.DB) *PartnerRepo {
	return &PartnerRepo{db}
}

func (r *PartnerRepo) GetAll() ([]models.Partner, error) {
	list := []models.Partner{}
	err := r.DB.Find(&list).Error
	if err != nil {
		return nil, err
	}
	return list, nil
}
func (r *PartnerRepo) GetById(id int64) (models.Partner, error) {
	obj := models.Partner{}
	err := r.DB.First(&obj, id).Error
	if err != nil {
		return models.Partner{}, err
	}
	return obj, nil
}
func (r *PartnerRepo) Create(pc models.Partner) error {
	addRc := r.DB.NewRecord(pc)
	if addRc {
		err := r.DB.Create(&pc).Error
		if err != nil {
			return err
		}
	}
	return nil
}
func (r *PartnerRepo) Update(id int64, updateInfo map[string]interface{}) error {
	pc, err := r.GetById(id)
	if err != nil {
		return err
	}
	err = r.DB.Model(&pc).UpdateColumns(updateInfo).Error
	if err != nil {
		return err
	}
	return nil
}
func (r *PartnerRepo) Delete(id int64) error {
	pc, err := r.GetById(id)
	if err != nil {
		return err
	}
	err = r.DB.Delete(&pc).Error
	if err != nil {
		return err
	}
	return nil
}
