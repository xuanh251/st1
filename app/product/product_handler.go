package product

import (
	"encoding/json"
	"github.com/go-chi/chi"
	"github.com/lib/pq"
	"io/ioutil"
	"net/http"
	"st1v2/configs/database"
	"st1v2/models"
	"st1v2/utils"
	"strconv"
	"strings"
)

type ProductHandler struct {
	service IProductService
}

func NewHTTPHandler(db *database.SQL) *ProductHandler {
	r := NewRepository(db.DB)
	u := NewService(db.DB, r)
	return &ProductHandler{u}
}

//Product Category
func (h *ProductHandler) GetAllPC(w http.ResponseWriter, r *http.Request) {
	rs, err := h.service.CategoryGetAll()
	if err != nil {
		utils.RespondWithError(w, http.StatusUnprocessableEntity, err.Error())
		return
	}
	utils.RespondWithJSON(w, 200, rs)
}
func (h *ProductHandler) GetPCById(w http.ResponseWriter, r *http.Request) {
	id, err := strconv.ParseInt(chi.URLParam(r, "id"), 10, 64)
	if err != nil {
		utils.RespondWithError(w, http.StatusUnprocessableEntity, err.Error())
		return
	}
	pc, err := h.service.CategoryGetById(id)
	if err != nil {
		utils.RespondWithError(w, http.StatusUnprocessableEntity, "Data error"+err.Error())
		return
	}
	utils.RespondWithJSON(w, http.StatusOK, pc)
}
func (h *ProductHandler) CreatePC(w http.ResponseWriter, r *http.Request) {
	pc := models.ProductCategory{}
	body, _ := ioutil.ReadAll(r.Body)
	err := json.Unmarshal(body, &pc)
	if err != nil {
		utils.RespondWithError(w, http.StatusBadRequest, err.Error())
		return
	}
	err = h.service.CategoryCreate(pc)
	if err != nil {
		utils.RespondWithError(w, http.StatusUnprocessableEntity, err.(*pq.Error).Detail)
		return
	}
	utils.RespondWithJSON(w, 201, "Create successfully!")
}
func (h *ProductHandler) UpdatePc(w http.ResponseWriter, r *http.Request) {
	id, err := strconv.ParseInt(chi.URLParam(r, "id"), 10, 64)
	if err != nil {
		utils.RespondWithError(w, http.StatusUnprocessableEntity, err.Error())
		return
	}
	var updateInfo map[string]interface{}
	body, _ := ioutil.ReadAll(r.Body)
	err = json.Unmarshal(body, &updateInfo)
	if err != nil {
		utils.RespondWithError(w, http.StatusBadRequest, err)
		return
	}
	err = h.service.CategoryUpdate(id, updateInfo)
	if err != nil {
		utils.RespondWithError(w, http.StatusUnprocessableEntity, err.(*pq.Error).Detail)
		return
	}
	utils.RespondWithJSON(w, http.StatusOK, "Cập nhật thành công")
}
func (h *ProductHandler) DeletePC(w http.ResponseWriter, r *http.Request) {
	id, err := strconv.ParseInt(chi.URLParam(r, "id"), 10, 64)
	if err != nil {
		utils.RespondWithError(w, http.StatusUnprocessableEntity, err.Error())
		return
	}
	err = h.service.CategoryDelete(id)
	if err != nil {
		utils.RespondWithError(w, http.StatusUnprocessableEntity, err.Error())
		return
	}
	utils.RespondWithJSON(w, http.StatusOK, "Xoá thành công!")
}

//Product
func (h *ProductHandler) GetAll(w http.ResponseWriter, r *http.Request) {
	//var requestData map[string]string
	//body, _ := ioutil.ReadAll(r.Body)
	//err := json.Unmarshal(body, &requestData)
	//if err != nil {
	//	utils.RespondWithError(w, http.StatusBadRequest, err.Error())
	//	return
	//}
	//queryData := requestData["query"]
	//if queryData == "" {
	//	utils.RespondWithError(w, http.StatusBadRequest, err.Error())
	//	return
	//}
	//requestField := strings.Split(queryData, ", ")
	rs, err := h.service.GetAll()
	if err != nil {
		utils.RespondWithError(w, http.StatusUnprocessableEntity, err.Error())
		return
	}
	utils.RespondWithJSON(w, 200, rs)
}
func (h *ProductHandler) GetById(w http.ResponseWriter, r *http.Request) {
	id, err := strconv.ParseInt(chi.URLParam(r, "id"), 10, 64)
	if err != nil {
		utils.RespondWithError(w, http.StatusUnprocessableEntity, err.Error())
		return
	}
	pc, err := h.service.GetById(id)
	if err != nil {
		utils.RespondWithError(w, http.StatusUnprocessableEntity, "Data error"+err.Error())
		return
	}
	utils.RespondWithJSON(w, http.StatusOK, pc)
}
func (h *ProductHandler) Create(w http.ResponseWriter, r *http.Request) {
	pc := models.Product{}
	body, _ := ioutil.ReadAll(r.Body)
	err := json.Unmarshal(body, &pc)
	if err != nil {
		utils.RespondWithError(w, http.StatusBadRequest, err.Error())
		return
	}
	err = h.service.Create(pc)
	if err != nil {
		utils.RespondWithError(w, http.StatusUnprocessableEntity, err.(*pq.Error).Detail)
		return
	}
	utils.RespondWithJSON(w, 201, "Create successfully!")
}
func (h *ProductHandler) Update(w http.ResponseWriter, r *http.Request) {
	id, err := strconv.ParseInt(chi.URLParam(r, "id"), 10, 64)
	if err != nil {
		utils.RespondWithError(w, http.StatusUnprocessableEntity, err.Error())
		return
	}
	var updateInfo map[string]interface{}
	body, _ := ioutil.ReadAll(r.Body)
	err = json.Unmarshal(body, &updateInfo)
	if err != nil {
		utils.RespondWithError(w, http.StatusBadRequest, err)
		return
	}
	err = h.service.Update(id, updateInfo)
	if err != nil {
		utils.RespondWithError(w, http.StatusUnprocessableEntity, err.(*pq.Error).Detail)
		return
	}
	utils.RespondWithJSON(w, http.StatusOK, "Cập nhật thành công")
}
func (h *ProductHandler) Delete(w http.ResponseWriter, r *http.Request) {
	id, err := strconv.ParseInt(chi.URLParam(r, "id"), 10, 64)
	if err != nil {
		utils.RespondWithError(w, http.StatusUnprocessableEntity, err.Error())
		return
	}
	err = h.service.Delete(id)
	if err != nil {
		utils.RespondWithError(w, http.StatusUnprocessableEntity, "Data error"+err.Error())
		return
	}
	utils.RespondWithJSON(w, http.StatusOK, "Xoá thành công!")
}
