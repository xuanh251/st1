package product

import (
	"github.com/jinzhu/gorm"
	"st1v2/models"
)

type IProduct interface {
	GetAll() ([]models.Product, error)
	GetById(id int64) (models.Product, error)
	Create(pc models.Product) error
	Update(id int64, updateInfo map[string]interface{}) error
	Delete(id int64) error
	GetListChildCategory(id int64) ([]models.ProductCategory, error)

	CategoryGetAll() ([]models.ProductCategory, error)
	CategoryGetById(id int64) (models.ProductCategory, error)
	CategoryCreate(pc models.ProductCategory) error
	CategoryUpdate(id int64, updateInfo map[string]interface{}) error
	CategoryDelete(id int64) error
	GetListProductByCategory(catId int64) ([]models.Product, error)
}
type ProductRepo struct {
	DB *gorm.DB
}

func NewRepository(db *gorm.DB) *ProductRepo {
	return &ProductRepo{db}
}

//Product Category
func (r *ProductRepo) CategoryGetAll() ([]models.ProductCategory, error) {
	list := []models.ProductCategory{}
	err := r.DB.Find(&list).Error
	if err != nil {
		return nil, err
	}
	return list, nil
}
func (r *ProductRepo) CategoryGetById(id int64) (models.ProductCategory, error) {
	obj := models.ProductCategory{}
	err := r.DB.First(&obj, id).Error
	if err != nil {
		return models.ProductCategory{}, err
	}
	return obj, nil
}
func (r *ProductRepo) CategoryCreate(pc models.ProductCategory) error {
	addRc := r.DB.NewRecord(pc)
	if addRc {
		err := r.DB.Create(&pc).Error
		if err != nil {
			return err
		}
	}
	return nil
}
func (r *ProductRepo) CategoryUpdate(id int64, updateInfo map[string]interface{}) error {
	pc, err := r.CategoryGetById(id)
	if err != nil {
		return err
	}
	err = r.DB.Model(&pc).UpdateColumns(updateInfo).Error
	if err != nil {
		return err
	}
	return nil
}
func (r *ProductRepo) CategoryDelete(id int64) error {
	pc, err := r.CategoryGetById(id)
	if err != nil {
		return err
	}
	err = r.DB.Delete(&pc).Error
	if err != nil {
		return err
	}
	return nil
}
func (r *ProductRepo) GetListChildCategory(id int64) ([]models.ProductCategory, error) {
	list := []models.ProductCategory{}
	err := r.DB.Where("parent_id=?", id).Find(&list).Error
	if err != nil {
		return nil, err
	}
	return list, nil
}

//Product
func (r *ProductRepo) GetAll() ([]models.Product, error) {
	list := []models.Product{}
	err := r.DB.Preload("Category").Preload("UnitFirst").Preload("UnitSecond").Find(&list).Error
	if err != nil {
		return nil, err
	}
	return list, nil
}
func (r *ProductRepo) GetById(id int64) (models.Product, error) {
	obj := models.Product{}
	err := r.DB.First(&obj, id).Error
	if err != nil {
		return models.Product{}, err
	}
	return obj, nil
}
func (r *ProductRepo) Create(pc models.Product) error {
	addRc := r.DB.NewRecord(pc)
	if addRc {
		err := r.DB.Create(&pc).Error
		if err != nil {
			return err
		}
	}
	return nil
}
func (r *ProductRepo) Update(id int64, updateInfo map[string]interface{}) error {
	pc, err := r.GetById(id)
	if err != nil {
		return err
	}
	err = r.DB.Model(&pc).UpdateColumns(updateInfo).Error
	if err != nil {
		return err
	}
	return nil
}
func (r *ProductRepo) Delete(id int64) error {
	pc, err := r.GetById(id)
	if err != nil {
		return err
	}
	err = r.DB.Delete(&pc).Error
	if err != nil {
		return err
	}
	return nil
}
func (r *ProductRepo) GetListProductByCategory(catId int64) ([]models.Product, error) {
	list := []models.Product{}
	err := r.DB.Where("category_id=?", catId).Find(&list).Error
	if err != nil {
		return nil, err
	}
	return list, nil
}
