package product

import (
	"errors"
	"github.com/jinzhu/gorm"
	"sort"
	"st1v2/models"
)

type IProductService interface {
	CategoryGetAll() (interface{}, error)
	CategoryGetById(id int64) (interface{}, error)
	CategoryCreate(user models.ProductCategory) error
	CategoryUpdate(id int64, updateInfo map[string]interface{}) error
	CategoryDelete(id int64) error

	GetAll() (interface{}, error)
	GetById(id int64) (interface{}, error)
	Create(user models.Product) error
	Update(id int64, updateInfo map[string]interface{}) error
	Delete(id int64) error
}
type ProductService struct {
	db         *gorm.DB
	repository IProduct
}

func NewService(db *gorm.DB, r IProduct) *ProductService {
	return &ProductService{db, r}
}

//Product Category
func (s *ProductService) CategoryGetAll() (interface{}, error) {
	pc, err := s.repository.CategoryGetAll()
	if err != nil {
		return nil, err
	}
	a := models.ProductCategory{Id: -1, Name: "Nhóm cha"}
	pc = append(pc, a)
	sort.SliceStable(pc, func(i, j int) bool {
		return pc[i].Id < pc[j].Id
	})
	return pc, nil
}

func (s *ProductService) CategoryGetById(id int64) (interface{}, error) {
	return s.repository.CategoryGetById(id)
}

func (s *ProductService) CategoryCreate(data models.ProductCategory) error {
	return s.repository.CategoryCreate(data)
}

func (s *ProductService) CategoryUpdate(id int64, updateInfo map[string]interface{}) error {
	return s.repository.CategoryUpdate(id, updateInfo)
}

func (s *ProductService) CategoryDelete(id int64) error {
	list, err := s.repository.GetListProductByCategory(id)
	if err != nil {
		return err
	}
	if len(list) > 0 {
		return errors.New("Nhóm này đang chứa sản phẩm, bạn không thể xoá!")
	}
	listCat, err := s.repository.GetListChildCategory(id)
	if err != nil {
		return err
	}
	if len(listCat) > 0 {
		return errors.New("Nhóm này đang chứa nhóm khác, bạn không thể xoá!")
	}
	return s.repository.CategoryDelete(id)
}

//Product
func (s *ProductService) GetAll() (interface{}, error) {
	//var rs []interface{}
	//pc, err := s.repository.GetAll()
	//if err != nil {
	//	return nil, err
	//}
	//if opt[0] != "*" {
	//	for _, elem := range pc {
	//		m := utils.CustomFeild(elem, opt)
	//		if len(m) == 0 {
	//			return nil, errors.New("Bad request payload")
	//		}
	//		rs = append(rs, m)
	//	}
	//	return rs, nil
	//}
	return s.repository.GetAll()
}

func (s *ProductService) GetById(id int64) (interface{}, error) {
	return s.repository.GetById(id)
}

func (s *ProductService) Create(data models.Product) error {
	return s.repository.Create(data)
}

func (s *ProductService) Update(id int64, updateInfo map[string]interface{}) error {
	return s.repository.Update(id, updateInfo)
}

func (s *ProductService) Delete(id int64) error {
	return s.repository.Delete(id)
}
