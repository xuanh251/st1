package export

import (
	"github.com/jinzhu/gorm"
	"st1v2/models"
	"st1v2/utils"
	"strings"
)

type IExport interface {
	GetAllWithPagination(pageIndex, pageSize int, searchString string) (interface{}, error)
	GetById(id int64) (models.Export, error)
	Create(export models.Export) error
	Update(id int64, updateInfo map[string]interface{}) error
	Delete(id int64) error
	GetAdditionalInfo() (interface{}, error)
	GetExportByBillNumber(billNumber string) (interface{}, error)
}
type Repo struct {
	DB *gorm.DB
}

func NewRepository(db *gorm.DB) *Repo {
	return &Repo{db}
}

func (r *Repo) Create(export models.Export) error {
	addRc := r.DB.NewRecord(export)
	if addRc {
		err := r.DB.Create(&export).Error
		if err != nil {
			return err
		}
	}
	return nil
}
func (r *Repo) GetTableCount() (int64, error) {
	var count int64 = 0
	err := r.DB.Model(&models.Export{}).Count(&count).Error
	if err != nil {
		return 0, err
	}
	return count, nil
}
func (r *Repo) GetAdditionalInfo() (interface{}, error) {
	result := models.ExportGlobalInfoViewModel{}
	var total float64
	var discount float64
	var paid float64
	err := r.DB.Model(models.ExportDetail{}).Select("sum(price*amount*(1-discount_percent)) as total").Row().Scan(&total)
	if err != nil {
		return nil, err
	}
	result.TotalMoney = total
	subqr := r.DB.Model(models.Export{}).Select("(sum(export_details.price*export_details.amount*(1- export_details.discount_percent/100))*(exports.discount_percent/100)) as export_discount_money").
		Joins("JOIN export_details on exports.id=export_details.bill_id").
		Group(" exports.bill_number, exports.discount_percent").
		SubQuery()
	err = r.DB.Raw("select sum(a.export_discount_money) from ? as a", subqr).Row().Scan(&discount)
	//err = r.DB.Model(models.Export{}).Select("sum(price*amount*(discount_percent)) as total").Row().Scan(&discount)
	if err != nil {
		return nil, err
	}
	result.TotalDiscount = discount

	err = r.DB.Model(models.Receipt{}).Select("sum(receipts.paid_money) as paid").
		Joins("JOIN exports on exports.id=receipts.export_bill_id").Row().Scan(&paid)
	result.TotalPaid = paid
	return result, nil
}

func (r *Repo) GetAllWithPagination(pageIndex, pageSize int, searchString string) (interface{}, error) {
	searchString = strings.ToLower(searchString)
	//var ListDetail []models.ExportDetailViewModel
	total := 0
	rows, err := r.DB.Model(models.Export{}).
		Select(`exports.bill_number, exports.created_at, partners.name as partner_name,
sum(export_details.amount*export_details.price*(1- export_details.discount_percent/100)) as total,
(sum(export_details.amount*export_details.price*(1- export_details.discount_percent/100))*exports.discount_percent/100) as discount, 
receipts.paid_money`).
		Joins("JOIN export_details on exports.id = export_details.bill_id").
		Joins("JOIN receipts on exports.id=receipts.export_bill_id").
		Joins("JOIN partners on exports.partner_id = partners.id").
		Where("lower(exports.bill_number) LIKE ?", "%"+searchString+"%").Or("lower(partners.name) LIKE ?", "%"+searchString+"%").
		Group("exports.bill_number, exports.created_at, receipts.paid_money, exports.discount_percent, partners.name").
		Order("exports.created_at").Count(&total).
		Offset(pageSize * (pageIndex - 1)).
		Limit(pageSize).Rows()
	if err != nil {
		return nil, err
	}
	list, err := utils.TransferData(rows, true)
	if err != nil {
		return nil, err
	}
	rs := models.ExportPaginationViewModel{TotalRecord: total, ExportDetail: list}
	return rs, nil
}

func (r *Repo) GetExportByBillNumber(billNumber string) (interface{}, error) {
	rows, err := r.DB.Model(models.Export{}).
		Select("exports.bill_number, exports.created_at, partners.name as partner_name, exports.status, users.full_name as user, exports.content, exports.note, sum(export_details.price*(1-export_details.discount_percent/100)*export_details.amount) as total, (sum(export_details.price*export_details.amount*(1-export_details.discount_percent/100))*exports.discount_percent/100) as discount, receipts.paid_money").
		Joins("JOIN partners on exports.partner_id = partners.id").
		Joins("JOIN users on users.id = exports.created_by").
		Joins("JOIN export_details on exports.id = export_details.bill_id").
		Joins("JOIN receipts on receipts.export_bill_id = exports.id").
		Where("exports.bill_number=?", billNumber).
		Group("exports.bill_number, exports.created_at, partners.name, exports.status, users.full_name, exports.content, exports.note, receipts.paid_money,exports.discount_percent").Rows()
	if err != nil {
		return nil, err
	}
	info, err := utils.TransferData(rows, false)
	if err != nil {
		return nil, err
	}
	rows, err = r.DB.Model(models.ExportDetail{}).
		Select("export_details.product_id, products.name, units.name as unit_name, export_details.amount, export_details.price, (export_details.price*export_details.discount_percent/100) as discount, (export_details.price*(1-export_details.discount_percent/100)) as new_price, (export_details.price*(1-export_details.discount_percent/100)*export_details.amount) as payment").
		Joins("JOIN products on products.id=export_details.id").
		Joins("JOIN exports on exports.id = export_details.bill_id").
		Joins(" JOIN units on units.id=products.unit_id_first").
		Where(" exports.bill_number=?", billNumber).Rows()
	if err != nil {
		return nil, err
	}
	list, err := utils.TransferData(rows, true)
	if err != nil {
		return nil, err
	}
	rs := models.ExportInfoViewModel{Info: info, ListDetail: list}
	return rs, nil
}

func (r *Repo) GetById(id int64) (models.Export, error) {
	obj := models.Export{}
	err := r.DB.First(&obj, id).Error
	if err != nil {
		return models.Export{}, err
	}
	return obj, nil
}
func (r *Repo) Update(id int64, updateInfo map[string]interface{}) error {
	pc, err := r.GetById(id)
	if err != nil {
		return err
	}
	err = r.DB.Model(&pc).UpdateColumns(updateInfo).Error
	if err != nil {
		return err
	}
	return nil
}
func (r *Repo) Delete(id int64) error {
	pc, err := r.GetById(id)
	if err != nil {
		return err
	}
	err = r.DB.Delete(&pc).Error
	if err != nil {
		return err
	}
	return nil
}
