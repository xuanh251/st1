package export

import (
	"github.com/jinzhu/gorm"
	"st1v2/models"
)

type IExportService interface {
	GetAll(pageIndex, pageSize int, searchString string) (interface{}, error)
	GetById(id int64) (interface{}, error)
	Create(export models.Export) error
	Update(id int64, updateInfo map[string]interface{}) error
	Delete(id int64) error
	GetAdditionalInfo() (interface{}, error)
	GetExportByBillNumber(billNumber string) (interface{}, error)
}
type ExportService struct {
	db         *gorm.DB
	repository IExport
}

func NewService(db *gorm.DB, r IExport) *ExportService {
	return &ExportService{db, r}
}
func (s *ExportService) Create(export models.Export) error {
	return s.repository.Create(export)
}
func (s *ExportService) GetAll(pageIndex, pageSize int, searchString string) (interface{}, error) {
	list, err := s.repository.GetAllWithPagination(pageIndex, pageSize, searchString)
	if err != nil {
		return nil, err
	}
	return list, nil
}
func (s *ExportService) GetById(id int64) (interface{}, error) {
	return s.repository.GetById(id)
}
func (s *ExportService) Update(id int64, updateInfo map[string]interface{}) error {
	return s.repository.Update(id, updateInfo)
}

func (s *ExportService) Delete(id int64) error {
	return s.repository.Delete(id)
}
func (s *ExportService) GetAdditionalInfo() (interface{}, error) {
	return s.repository.GetAdditionalInfo()
}
func (s *ExportService) GetExportByBillNumber(billNumber string) (interface{}, error) {
	return s.repository.GetExportByBillNumber(billNumber)
}
