package export

import (
	"encoding/json"
	"github.com/go-chi/chi"
	"github.com/lib/pq"
	"io/ioutil"
	"net/http"
	"st1v2/configs/database"
	"st1v2/models"
	"st1v2/utils"
	"strconv"
)

type Handler struct {
	service IExportService
}

func NewHTTPHandler(db *database.SQL) *Handler {
	r := NewRepository(db.DB)
	u := NewService(db.DB, r)
	return &Handler{u}
}

func (h *Handler) Create(w http.ResponseWriter, r *http.Request) {
	obj := models.Export{}
	body, _ := ioutil.ReadAll(r.Body)
	err := json.Unmarshal(body, &obj)
	if err != nil {
		utils.RespondWithError(w, http.StatusBadRequest, err.Error())
		return
	}
	err = h.service.Create(obj)
	if err != nil {
		utils.RespondWithError(w, http.StatusUnprocessableEntity, err.(*pq.Error).Detail)
		return
	}
	utils.RespondWithJSON(w, http.StatusCreated, "Create successfully!")
}
func (h *Handler) GetAll(w http.ResponseWriter, r *http.Request) {
	pageSize, err := strconv.ParseInt(chi.URLParam(r, "pageSize"), 10, 64)
	if err != nil {
		utils.RespondWithError(w, http.StatusUnprocessableEntity, err.Error())
		return
	}
	pageIndex, err := strconv.ParseInt(chi.URLParam(r, "pageIndex"), 10, 64)
	if err != nil {
		utils.RespondWithError(w, http.StatusUnprocessableEntity, err.Error())
		return
	}
	searchString := chi.URLParam(r, "searchString")
	if searchString == "''" {
		searchString = ""
	}
	rs, err := h.service.GetAll(int(pageIndex), int(pageSize), searchString)
	if err != nil {
		utils.RespondWithError(w, http.StatusUnprocessableEntity, err.Error())
		return
	}
	utils.RespondWithJSON(w, http.StatusOK, rs)
}

func (h *Handler) GetExportByBillNumber(w http.ResponseWriter, r *http.Request) {
	billNumber := chi.URLParam(r, "bill")
	rs, err := h.service.GetExportByBillNumber(billNumber)
	if err != nil {
		utils.RespondWithError(w, http.StatusUnprocessableEntity, err.Error())
		return
	}
	utils.RespondWithJSON(w, http.StatusOK, rs)
}

func (h *Handler) GetById(w http.ResponseWriter, r *http.Request) {
	id, err := strconv.ParseInt(chi.URLParam(r, "id"), 10, 64)
	if err != nil {
		utils.RespondWithError(w, http.StatusUnprocessableEntity, err.Error())
		return
	}
	pc, err := h.service.GetById(id)
	if err != nil {
		utils.RespondWithError(w, http.StatusUnprocessableEntity, "Data error"+err.Error())
		return
	}
	utils.RespondWithJSON(w, http.StatusOK, pc)
}

func (h *Handler) Update(w http.ResponseWriter, r *http.Request) {
	id, err := strconv.ParseInt(chi.URLParam(r, "id"), 10, 64)
	if err != nil {
		utils.RespondWithError(w, http.StatusUnprocessableEntity, err.Error())
		return
	}
	var updateInfo map[string]interface{}
	body, _ := ioutil.ReadAll(r.Body)
	err = json.Unmarshal(body, &updateInfo)
	if err != nil {
		utils.RespondWithError(w, http.StatusBadRequest, err)
		return
	}
	err = h.service.Update(id, updateInfo)
	if err != nil {
		utils.RespondWithError(w, http.StatusUnprocessableEntity, err.(*pq.Error).Detail)
		return
	}
	utils.RespondWithJSON(w, http.StatusOK, "Cập nhật thành công")
}
func (h *Handler) Delete(w http.ResponseWriter, r *http.Request) {
	id, err := strconv.ParseInt(chi.URLParam(r, "id"), 10, 64)
	if err != nil {
		utils.RespondWithError(w, http.StatusUnprocessableEntity, err.Error())
		return
	}
	err = h.service.Delete(id)
	if err != nil {
		utils.RespondWithError(w, http.StatusUnprocessableEntity, "Data error"+err.Error())
		return
	}
	utils.RespondWithJSON(w, http.StatusOK, "Xoá thành công!")
}
func (h *Handler) GetAdditionalInfo(w http.ResponseWriter, r *http.Request) {
	data, err := h.service.GetAdditionalInfo()
	if err != nil {
		utils.RespondWithError(w, http.StatusUnprocessableEntity, err.Error())
		return
	}
	utils.RespondWithJSON(w, http.StatusOK, data)
}
