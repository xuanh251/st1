package unit

import (
	"github.com/jinzhu/gorm"
	"st1v2/models"
)

type IUnit interface {
	GetAll() ([]models.Unit, error)
	GetById(id int64) (models.Unit, error)
	Create(pc models.Unit) error
	Update(id int64, updateInfo map[string]interface{}) error
	Delete(id int64) error
}
type UnitRepo struct {
	DB *gorm.DB
}

func NewRepository(db *gorm.DB) *UnitRepo {
	return &UnitRepo{db}
}

func (r *UnitRepo) GetAll() ([]models.Unit, error) {
	list := []models.Unit{}
	err := r.DB.Find(&list).Error
	if err != nil {
		return nil, err
	}
	return list, nil
}
func (r *UnitRepo) GetById(id int64) (models.Unit, error) {
	obj := models.Unit{}
	err := r.DB.First(&obj, id).Error
	if err != nil {
		return models.Unit{}, err
	}
	return obj, nil
}
func (r *UnitRepo) Create(pc models.Unit) error {
	addRc := r.DB.NewRecord(pc)
	if addRc {
		err := r.DB.Create(&pc).Error
		if err != nil {
			return err
		}
	}
	return nil
}
func (r *UnitRepo) Update(id int64, updateInfo map[string]interface{}) error {
	pc, err := r.GetById(id)
	if err != nil {
		return err
	}
	err = r.DB.Model(&pc).UpdateColumns(updateInfo).Error
	if err != nil {
		return err
	}
	return nil
}
func (r *UnitRepo) Delete(id int64) error {
	pc, err := r.GetById(id)
	if err != nil {
		return err
	}
	err = r.DB.Delete(&pc).Error
	if err != nil {
		return err
	}
	return nil
}
