package unit

import (
	"errors"
	"github.com/jinzhu/gorm"
	"st1v2/models"
	"st1v2/utils"
)

type IUnitService interface {
	GetAll(opt []string) (interface{}, error)
	GetById(id int64) (interface{}, error)
	Create(unit models.Unit) error
	Update(id int64, updateInfo map[string]interface{}) error
	Delete(id int64) error
}
type UnitService struct {
	db         *gorm.DB
	repository IUnit
}

func NewService(db *gorm.DB, r IUnit) *UnitService {
	return &UnitService{db, r}
}

func (s *UnitService) GetAll(opt []string) (interface{}, error) {
	var rs []interface{}
	pc, err := s.repository.GetAll()
	if err != nil {
		return nil, err
	}
	if opt[0] != "*" {
		for _, elem := range pc {
			m := utils.CustomFeild(elem, opt)
			if len(m) == 0 {
				return nil, errors.New("Bad request payload")
			}
			rs = append(rs, m)
		}
		return rs, nil
	}
	return pc, nil
}

func (s *UnitService) GetById(id int64) (interface{}, error) {
	return s.repository.GetById(id)
}

func (s *UnitService) Create(data models.Unit) error {
	return s.repository.Create(data)
}

func (s *UnitService) Update(id int64, updateInfo map[string]interface{}) error {
	return s.repository.Update(id, updateInfo)
}

func (s *UnitService) Delete(id int64) error {
	return s.repository.Delete(id)
}
