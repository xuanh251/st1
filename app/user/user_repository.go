package user

import (
	"errors"
	"fmt"
	"github.com/jinzhu/gorm"
	"st1v2/models"
	"st1v2/utils"
	"time"
)

const (
	INACTIVE_ROLE int64 = 0
	DISABLED_ROLE int64 = -1
)

type IUser interface {
	GetById(id int64) (models.User, error)
	Login(login *models.UserLogin) (models.User, error)
	GetAll() ([]models.User, error)
	Create(user models.User) (models.User, error, int64)
	Update(id int64, updateInfo map[string]interface{}) error
	Delete(id int64) error
}

type UserRepo struct {
	DB *gorm.DB
}

func NewRepository(db *gorm.DB) *UserRepo {
	return &UserRepo{db}
}

func (r *UserRepo) GetById(id int64) (models.User, error) {
	u := models.User{}
	err := r.DB.Preload("Role").Find(&u, id).Error
	if err != nil {
		return models.User{}, err
	}
	return u, nil
}

func (r *UserRepo) Login(login *models.UserLogin) (models.User, error) {
	u := models.User{}
	err := r.DB.Where("email=?", login.Account).Or("user_name=?", login.Account).Or("phone=?", login.Account).Preload("Role").Find(&u).Error
	if err != nil {
		return models.User{}, err
	}
	err = utils.IsPassword(u.Password, login.Password)
	if err != nil {
		return models.User{}, errors.New("Wrong password!")
	}
	return u, nil
}

func (r *UserRepo) GetAll() ([]models.User, error) {
	listUser := []models.User{}
	t1 := time.Now()
	err := r.DB.Preload("Role").Find(&listUser).Error
	fmt.Println(time.Since(t1))
	if err != nil {
		return nil, err
	}
	return listUser, nil
}
func (r *UserRepo) Create(user models.User) (models.User, error, int64) {
	var tempRole int64 = 0
	genPass := utils.GeneratePassword()
	hashPassword, err := utils.Bcrypt(genPass)
	if err != nil {
		return models.User{}, err, 0
	}
	user.Password = string(hashPassword)
	addRc := r.DB.NewRecord(user)
	if addRc {
		if user.RoleId != 1 && user.RoleId != 4 {
			tempRole = user.RoleId
			user.RoleId = 0
		}
		err := r.DB.Create(&user).Error
		if err != nil {
			return models.User{}, err, 0
		}
	}
	user.Password = genPass
	return user, nil, tempRole
}
func (r *UserRepo) Update(id int64, updateInfo map[string]interface{}) error {
	user, err := r.GetById(id)
	if err != nil {
		return err
	}
	err = r.DB.Model(&user).UpdateColumns(updateInfo).Error
	if err != nil {
		return err
	}
	return nil
}
func (r *UserRepo) Delete(id int64) error {
	user, err := r.GetById(id)
	if err != nil {
		return err
	}
	err = r.DB.Delete(&user).Error
	if err != nil {
		return err
	}
	return nil
}
