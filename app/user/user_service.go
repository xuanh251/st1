package user

import (
	"errors"
	"fmt"
	"github.com/dgrijalva/jwt-go"
	"github.com/jinzhu/gorm"
	"net/http"
	"st1v2/models"
	shareService "st1v2/share/services"
	"st1v2/utils"
)

type Auth struct {
	User  interface{} `json:"user"`
	Token string      `json:"token"`
}

type UserService struct {
	db         *gorm.DB
	repository IUser
}

func NewService(db *gorm.DB, r IUser) *UserService {
	return &UserService{db, r}
}

type IUserService interface {
	Login(user *models.UserLogin) (Auth, error)
	Create(user models.User, url string) error
	Update(id int64, updateInfo map[string]interface{}) error
	GetAll(opt []string) (interface{}, error)

	GetById(id int64) (models.User, error)
	Delete(id int64) error

	ConfirmMail(rawToken string) (statusCode int, message string)
	ResendConfirmMail(oldRawToken string) error
}

func (s *UserService) ConfirmMail(rawToken string) (statusCode int, message string) {
	token, err := jwt.Parse(rawToken,
		func(token *jwt.Token) (interface{}, error) {
			if _, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
				return nil, fmt.Errorf("Wrong token!")
			}
			return utils.JwtKeySecret_ConfirmEmail, nil
		})
	userId := token.Claims.(jwt.MapClaims)["userId"].(float64)
	user, e := s.repository.GetById(int64(userId))
	if e != nil {
		return http.StatusUnprocessableEntity, "Đã xảy ra lỗi: " + e.Error()
	}
	if user.RoleId == -1 {
		return http.StatusForbidden, "Tài khoản này đã bị vô hiệu hoá bởi quản trị!"
	}
	if err != nil {
		if err.Error() == "Token is expired" {
			return http.StatusForbidden, "Phiên xác thực đã hết hạn!"
		}
		return http.StatusForbidden, "Thông tin xác thực không chính xác!"
	}
	if token.Valid {
		roleId := token.Claims.(jwt.MapClaims)["roleId"].(interface{})
		userName := token.Claims.(jwt.MapClaims)["userName"].(string)
		err = s.repository.Update(int64(userId), map[string]interface{}{"role_id": roleId})
		if err != nil {
			return http.StatusUnprocessableEntity, "Lỗi: " + err.Error()
		}
		return http.StatusOK, userName
	} else {
		return http.StatusForbidden, "Thông tin xác thực không chính xác!"
	}
}

func (u *UserService) GetById(id int64) (models.User, error) {
	return u.repository.GetById(int64(id))
}

func (s *UserService) GetAll(opt []string) (interface{}, error) {
	var rs []interface{}
	users, err := s.repository.GetAll()
	if err != nil {
		return nil, err
	}
	if opt[0] != "*" {
		for _, elem := range users {
			m := utils.CustomFeild(elem, opt)
			if len(m) == 0 {
				return nil, errors.New("Bad request payload")
			}
			rs = append(rs, m)
		}
		return rs, nil
	}
	return users, nil
}

func (s *UserService) Login(user *models.UserLogin) (Auth, error) {
	_user, err := s.repository.Login(user)
	if err != nil {
		return Auth{}, err
	}
	if _user.Id == 0 {
		return Auth{}, errors.New("User not found!")
	}
	if _user.RoleId == INACTIVE_ROLE || _user.RoleId == DISABLED_ROLE {
		return Auth{}, errors.New("Access denied!")
	}
	token, err := utils.GenerateJWT_Login(_user)
	if err != nil {
		return Auth{}, err
	}
	return Auth{_user, token}, nil
}
func (s *UserService) Create(user models.User, baseUrl string) error {
	user, err, tempRole := s.repository.Create(user)
	if err != nil {
		return err
	}
	confirmToken := ""
	if user.RoleId == 0 {
		confirmToken, err = utils.GenerateJWT_ConfirmEmail(tempRole, user.Id, user.UserName, baseUrl)
		if err != nil {
			return err
		}
	}
	go shareService.SendEmail(user.Email, user.Password, confirmToken, baseUrl)
	return nil
}

func (s *UserService) ResendConfirmMail(oldRawToken string) error {
	token, _ := jwt.Parse(oldRawToken,
		func(token *jwt.Token) (interface{}, error) {
			if _, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
				return nil, fmt.Errorf("Wrong token!")
			}
			return utils.JwtKeySecret_ConfirmEmail, nil
		})
	userId := token.Claims.(jwt.MapClaims)["userId"].(float64)
	roleId := token.Claims.(jwt.MapClaims)["roleId"].(float64)
	baseUrl := token.Claims.(jwt.MapClaims)["fEBaseURl"].(string)
	user, err := s.repository.GetById(int64(userId))
	if err != nil {
		return err
	}
	newToken, err := utils.GenerateJWT_ConfirmEmail(int64(roleId), int64(userId), user.UserName, baseUrl)
	if err != nil {
		return err
	}
	go shareService.SendEmail(user.Email, "Xem trong mail cũ", newToken, baseUrl)
	return nil
}

func (s *UserService) Update(id int64, updateInfo map[string]interface{}) error {
	password := updateInfo["password"]
	confirmPassword := updateInfo["confirm_password"]
	if password != nil {
		if password == confirmPassword {
			encryptPass, err := utils.Bcrypt(password.(string))
			if err != nil {
				return err
			}
			updateInfo["password"] = string(encryptPass)
		} else {
			return errors.New("Password and confirm password not match!")
		}
	}
	return s.repository.Update(id, updateInfo)
}

func (u *UserService) Delete(id int64) error {
	return u.repository.Delete(id)
}
