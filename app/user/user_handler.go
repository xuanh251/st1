package user

import (
	"encoding/json"
	"errors"
	"github.com/go-chi/chi"
	"io/ioutil"
	"net/http"
	"st1v2/configs/database"
	"st1v2/configs/errorcode"
	"st1v2/models"
	"st1v2/utils"
	"strconv"
	"strings"
)

type UserHandler struct {
	service IUserService
}

func NewHTTPHandler(db *database.SQL) *UserHandler {
	r := NewRepository(db.DB)
	u := NewService(db.DB, r)
	return &UserHandler{u}
}

func (h *UserHandler) Login(w http.ResponseWriter, r *http.Request) {
	user := models.UserLogin{}
	decoder := json.NewDecoder(r.Body)
	if err := decoder.Decode(&user); err != nil {
		utils.RespondWithError(w, http.StatusBadRequest, err.Error())
		return
	}
	defer r.Body.Close()
	auth, err := h.service.Login(&user)
	if err != nil {
		utils.RespondWithError(w, http.StatusInternalServerError, utils.HandlerErrorFromDb(err))
		return
	}
	if auth.Token == "" {
		utils.RespondWithError(w, http.StatusUnauthorized, errorcode.WrongEmailPassword)
		return
	}
	utils.RespondWithJSON(w, http.StatusOK, auth)
}
func (h *UserHandler) Create(w http.ResponseWriter, r *http.Request) {
	var requestInfo map[string]interface{}
	user := models.User{}
	body, _ := ioutil.ReadAll(r.Body)
	err := json.Unmarshal(body, &requestInfo)
	if err != nil {
		utils.RespondWithError(w, http.StatusBadRequest, err.Error())
		return
	}
	url := requestInfo["domain"]
	if url == nil {
		utils.RespondWithError(w, http.StatusBadRequest, errors.New("No client URL submited").Error())
		return
	}
	err = json.Unmarshal(body, &user)
	if err != nil {
		utils.RespondWithError(w, http.StatusBadRequest, err.Error())
		return
	}
	user.UserName = strings.TrimSpace(user.UserName)
	user.Email = strings.TrimSpace(user.Email)
	user.Phone = strings.TrimSpace(user.Phone)
	err = h.service.Create(user, url.(string))
	if err != nil {
		utils.RespondWithError(w, http.StatusUnprocessableEntity, utils.HandlerErrorFromDb(err))
		return
	}
	utils.RespondWithJSON(w, 201, "Create successfully")
}

func (h *UserHandler) GetAll(w http.ResponseWriter, r *http.Request) {
	var requestData map[string]string
	body, _ := ioutil.ReadAll(r.Body)
	err := json.Unmarshal(body, &requestData)
	if err != nil {
		utils.RespondWithError(w, http.StatusBadRequest, err.Error())
		return
	}
	queryData := requestData["query"]
	if queryData == "" {
		utils.RespondWithError(w, http.StatusBadRequest, err.Error())
		return
	}
	requestField := strings.Split(queryData, ", ")
	rs, err := h.service.GetAll(requestField)
	if err != nil {
		utils.RespondWithError(w, http.StatusUnprocessableEntity, err.Error())
		return
	}
	utils.RespondWithJSON(w, 200, rs)
}
func (h *UserHandler) ConfirmMail(w http.ResponseWriter, r *http.Request) {
	mailAuthHeader := r.Header.Get("Mail-Auth")
	if mailAuthHeader != "" {
		statusCode, message := h.service.ConfirmMail(mailAuthHeader)
		if statusCode != http.StatusOK {
			utils.RespondWithError(w, statusCode, message)
			return
		}
		utils.RespondWithJSON(w, statusCode, message)
		return
	}
	utils.RespondWithError(w, http.StatusBadRequest, errors.New("Yêu cầu bị từ chối!").Error())
	return

}

func (h *UserHandler) ResendConfirmMail(w http.ResponseWriter, r *http.Request) {
	rawToken := chi.URLParam(r, "token")
	err := h.service.ResendConfirmMail(rawToken)
	if err != nil {
		utils.RespondWithError(w, http.StatusUnprocessableEntity, err.Error())
		return
	}
	utils.RespondWithJSON(w, http.StatusOK, "Gửi mail thành công!")
}

func (h *UserHandler) Update(w http.ResponseWriter, r *http.Request) {
	id, err := strconv.ParseInt(chi.URLParam(r, "id"), 10, 64)
	if err != nil {
		utils.RespondWithError(w, http.StatusUnprocessableEntity, err.Error())
		return
	}
	var updateInfo map[string]interface{}
	body, _ := ioutil.ReadAll(r.Body)
	err = json.Unmarshal(body, &updateInfo)
	if err != nil {
		utils.RespondWithError(w, http.StatusBadRequest, err)
		return
	}
	err = h.service.Update(id, updateInfo)
	if err != nil {
		utils.RespondWithError(w, http.StatusUnprocessableEntity, err.Error())
		return
	}
	utils.RespondWithJSON(w, http.StatusOK, "Cập nhật thành công")
}
func (h *UserHandler) GetById(w http.ResponseWriter, r *http.Request) {
	id, err := strconv.ParseInt(chi.URLParam(r, "id"), 10, 64)
	if err != nil {
		utils.RespondWithError(w, http.StatusUnprocessableEntity, err.Error())
		return
	}
	user, err := h.service.GetById(id)
	if err != nil {
		utils.RespondWithError(w, http.StatusUnprocessableEntity, "Data error: "+err.Error())
		return
	}
	utils.RespondWithJSON(w, http.StatusOK, user)
}
func (h *UserHandler) Delete(w http.ResponseWriter, r *http.Request) {
	id, err := strconv.ParseInt(chi.URLParam(r, "id"), 10, 64)
	if err != nil {
		utils.RespondWithError(w, http.StatusUnprocessableEntity, err.Error())
		return
	}
	err = h.service.Delete(id)
	if err != nil {
		utils.RespondWithError(w, http.StatusUnprocessableEntity, "Data error: "+err.Error())
		return
	}
	utils.RespondWithJSON(w, http.StatusOK, "Xoá thành công!")

}
