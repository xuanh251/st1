package app

import (
	"github.com/go-chi/chi"
	"github.com/go-chi/chi/middleware"
	"github.com/go-chi/cors"
	"log"
	"net/http"
	"st1v2/app/bill/export"
	"st1v2/app/partner"
	"st1v2/app/product"
	"st1v2/app/role"
	"st1v2/app/user"
	"st1v2/configs/database"
	guard "st1v2/middleware"
)

type App struct {
	Router     *chi.Mux
	SQLHandler *database.SQL
}

func (app *App) InitRoute() {
	app.Router = chi.NewRouter()
	app.SQLHandler = database.NewSQL()

	app.Router.Use(middleware.RequestID)
	app.Router.Use(middleware.RealIP)

	corsopt := cors.New(cors.Options{
		AllowedOrigins:   []string{"*"},
		AllowedMethods:   []string{"GET", "POST", "PUT", "DELETE", "OPTIONS"},
		AllowedHeaders:   []string{"Accept", "Authorization", "Content-Type", "X-CSRF-Token", "Mail-Auth"},
		AllowCredentials: true,
		MaxAge:           300, // Maximum value not ignored by any of major browsers
	})

	app.Router.Use(corsopt.Handler)
}

func (app *App) SetRouters() {
	//User
	userHandler := user.NewHTTPHandler(app.SQLHandler)
	roleHandler := role.NewHTTPHandler(app.SQLHandler)
	productHandler := product.NewHTTPHandler(app.SQLHandler)
	partnerHandler := partner.NewHTTPHandler(app.SQLHandler)
	exportHandler := export.NewHTTPHandler(app.SQLHandler)

	app.Router.Route("/api", func(r chi.Router) {
		r.Post("/auth/login", userHandler.Login)
	})
	app.Router.Route("/api/user", func(r chi.Router) {
		r.Post("/list", guard.IsAuth(userHandler.GetAll))
		r.Post("/create", guard.IsAuth(userHandler.Create, "SUPERADMIN", "ADMIN"))
		r.Get("/{id}", guard.IsAuth(userHandler.GetById))
		r.Put("/{id}", guard.IsAuth(userHandler.Update))
		r.Delete("/{id}", guard.IsAuth(userHandler.Delete))

		r.Post("/resend_email/{token}", userHandler.ResendConfirmMail)
		r.Post("/confirm_email", userHandler.ConfirmMail)
	})

	app.Router.Route("/api/role", func(r chi.Router) {
		r.Get("/list", guard.IsAuth(roleHandler.GetAll))
	})
	app.Router.Route("/api/product_category", func(r chi.Router) {
		r.Get("/list", guard.IsAuth(productHandler.GetAllPC))
		r.Post("/create", guard.IsAuth(productHandler.CreatePC))
		r.Get("/{id}", guard.IsAuth(productHandler.GetPCById))
		r.Put("/{id}", guard.IsAuth(productHandler.UpdatePc))
		r.Delete("/{id}", guard.IsAuth(productHandler.DeletePC))
	})
	app.Router.Route("/api/product", func(r chi.Router) {
		r.Get("/list", guard.IsAuth(productHandler.GetAll))
		r.Post("/create", guard.IsAuth(productHandler.Create))
		r.Get("/{id}", guard.IsAuth(productHandler.GetById))
		r.Put("/{id}", guard.IsAuth(productHandler.Update))
		r.Delete("/{id}", guard.IsAuth(productHandler.Delete))
	})
	app.Router.Route("/api/partner", func(r chi.Router) {
		r.Post("/list", guard.IsAuth(partnerHandler.GetAll))
		r.Post("/create", guard.IsAuth(partnerHandler.Create))
		r.Get("/{id}", guard.IsAuth(partnerHandler.GetById))
		r.Put("/{id}", guard.IsAuth(partnerHandler.Update))
		r.Delete("/{id}", guard.IsAuth(partnerHandler.Delete))
	})

	app.Router.Route("/api/bill", func(r chi.Router) {
		r.Route("/export", func(r chi.Router) {
			r.Post("/create", guard.IsAuth(exportHandler.Create))
			r.Get("/list/pageSize={pageSize}&pageIndex={pageIndex}&searchString={searchString}", exportHandler.GetAll)
			r.Get("/{id}", guard.IsAuth(exportHandler.GetById))
			r.Put("/{id}", guard.IsAuth(exportHandler.Update))
			r.Delete("/{id}", guard.IsAuth(exportHandler.Delete))
			r.Get("/getAdditionalInfo", guard.IsAuth(exportHandler.GetAdditionalInfo))
			r.Get("/GetInfoByBillNumber/bill={bill}", guard.IsAuth(exportHandler.GetExportByBillNumber))
		})
	})
}

func (app *App) Run() {
	app.InitRoute()
	app.SetRouters()

	log.Fatal(http.ListenAndServe(":3000", app.Router))
}
