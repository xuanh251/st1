package models

import (
	"time"
)

type Role struct {
	Id     int64  `gorm:"primary_key" json:"id"`
	Name   string `json:"name"`
	ViName string `json:"vi_name"`
}

func (Role) TableName() string {
	return "roles"
}

type User struct {
	Id       int64  `gorm:"primary_key" json:"id"`
	FullName string `json:"full_name"`
	UserName string `validate:"min=1" json:"user_name"`
	Email    string `json:"email"`
	Password string `json:"password"`
	Phone    string `json:"phone"`
	Address  string `json:"address"`
	RoleId   int64  `json:"role_id"`
	Role     Role   `json:"role"`
}

func (User) TableName() string {
	return "users"
}

type UserLogin struct {
	Account  string `json:"account"`
	Password string `json:"password"`
}

type ProductCategory struct {
	Id       int64  `gorm:"primary_key" json:"id"`
	Name     string `json:"name"`
	ParentId int64  `json:"parent_id"`
}

func (ProductCategory) TableName() string {
	return "product_categories"
}

type Product struct {
	Id             int64           `gorm:"primary_key" json:"id"`
	Name           string          `json:"name"`
	CategoryId     int64           `json:"category_id"`
	Category       ProductCategory `json:"category"`
	ImportPrice    int64           `json:"import_price"`
	RetailPrice    int64           `json:"retail_price"`
	WholesalePrice int64           `json:"wholesale_price"`
	Active         bool            `json:"active"`
	Sku            string          `json:"sku"`
	UnitIdFirst    int64           `json:"unit_id_first"`
	UnitFirst      Unit            `gorm:"ForeignKey:UnitIdFirst" json:"unit_first"`
	UnitIdSecond   int64           `json:"unit_id_second"`
	UnitSecond     Unit            `gorm:"ForeignKey:UnitIdSecond" json:"unit_second"`
	Exchange1      int64           `json:"exchange_1"`
	Exchange2      int64           `json:"exchange_2"`
}

func (Product) TableName() string {
	return "products"
}

type Unit struct {
	Id   int64  `gorm:"primary_key" json:"id"`
	Name string `json:"name"`
}

func (Unit) TableName() string {
	return "units"
}

type Partner struct {
	Id          int64  `gorm:"primary_key" json:"id"`
	Name        string `json:"name"`
	Address     string `json:"address"`
	PhoneNumber string `json:"phone_number"`
	Active      bool   `json:"active"`
	Groups      string `json:"groups"`
	Wholesale   bool   `json:"wholesale"`
	Tag         string `json:"tag"`
}

func (Partner) TableName() string {
	return "partners"
}

type Export struct {
	Id              int64     `gorm:"primary_key" json:"id"`
	CreatedAt       time.Time `json:"created_at"`
	PartnerId       int64     `json:"partner_id"`
	Partner         Partner   `gorm:"ForeignKey:PartnerId" json:"partner"`
	Content         string    `json:"content"`
	BillNumber      string    `json:"bill_number"`
	CreatedBy       int64     `json:"created_by"`
	User            User      `gorm:"ForeignKey:CreatedBy" json:"user"`
	Note            string    `json:"note"`
	Status          bool      `json:"status"`
	DiscountPercent float64   `json:"discount_percent"`
	IsPercent       bool      `json:"is_percent"`
}

func (Export) TableName() string {
	return "exports"
}

type ExportDetail struct {
	Id              int64   `gorm:"primary_key" json:"id"`
	BillId          int64   `json:"bill_id"`
	Export          Export  `gorm:"ForeignKey:BillId" json:"export"`
	ProductId       int64   `json:"product_id"`
	Product         Product `gorm:"ForeignKey:ProductId" json:"product"`
	Amount          int64   `json:"amount"`
	Price           float64 `json:"price"`
	OriginPrice     float64 `json:"origin_price"`
	DiscountPercent float64 `json:"discount_percent"`
}

func (ExportDetail) TableName() string {
	return "export_details"
}

type Receipt struct {
	Id           int64     `gorm:"primary_key" json:"id"`
	Content      string    `json:"content"`
	CreatedAt    time.Time `json:"created_at"`
	PartnerId    int64     `json:"partner_id"`
	Partner      Partner   `json:"partner"`
	PaidMoney    float64   `json:"paid_money"`
	Method       string    `json:"method"`
	CreatedBy    int64     `json:"created_by"`
	ExportBillId int64     `json:"export_bill_id"`
	Export       Export    `json:"export"`
}

func (Receipt) TableName() string {
	return "receipts"
}
