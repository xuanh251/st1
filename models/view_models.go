package models

type ExportGlobalInfoViewModel struct {
	TotalMoney    interface{} `json:"total_money"`
	TotalDiscount interface{} `json:"total_discount"`
	TotalPaid     interface{} `json:"total_paid"`
}
type ExportPaginationViewModel struct {
	TotalRecord  interface{} `json:"total_record"`
	ExportDetail interface{} `json:"export_detail"`
}
type ExportInfoViewModel struct {
	Info       interface{} `json:"info"`
	ListDetail interface{} `json:"list_detail"`
}
