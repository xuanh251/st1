/* roles */
create TABLE roles
(
    id         bigserial NOT NULL,
    name       varchar   NOT NULL CHECK (name <> ''),
    deleted_at timestamp,
    PRIMARY KEY (id)
);
/* users */
create table users
(
    id           bigserial not null CHECK (id > 1),
    user_name    varchar UNIQUE CHECK (user_name <> ''),
    email        varchar UNIQUE CHECK (email <> ''),
    password     varchar   not null CHECK (char_length(password) > 4),
    full_name    varchar   not null CHECK (full_name <> ''),
    phone_number varchar   not null CHECK (phone_number <> ''),
    role_id      int       NOT NULL,
    deleted_at   timestamp,
    PRIMARY KEY (id),
    CONSTRAINT user_role_fk FOREIGN KEY (role_id) REFERENCES roles (id)
);

/* unit */
create table units
(
    id   bigserial not null,
    name varchar   not null CHECK (name <> ''),
    PRIMARY Key (id)
);
/*product_category*/
create table product_category
(
    id        bigserial not null,
    name      varchar   not null CHECK (name <> ''),
    parent_id int,
    PRIMARY key (id)
);
/*return_bills*/
create table return_bills
(
    id             bigserial not null,
    created_at     int8      not null,
    partner_id     int       not null,
    export_bill_id int       not null,
    content        text,
    fees           int       not null,
    payments       int       not null,
    user_id        int       not null,
    warehouse_id   int       not null,
    status         varchar   not null,
    deleted_at     timestamp,
    PRIMARY key (id)
);
/*return_bills_detail*/
create table return_bills_detail
(
    id               bigserial not null,
    return_bill_id   int       not null,
    product_id       int       not null,
    amount           int       not null,
    price            int       not null,
    discount_percent float     not null,
    discount_money   int       not null,
    deleted_at       timestamp,
    PRIMARY key (id)
    /* Khóa ngoại*/
);
/*products*/
create table products
(
    id              bigserial not null,
    name            varchar   not null check ( name <> ''),
    category_id     int       not null,
    import_price    varchar   not null,
    retail_price    int       not null,
    wholesale_price int       not null,
    active          boolean   not null,
    SKU             int       not null,
    unit_id_1       int       not null,
    unit_id_2       int       not null,
    exchange_1      int,
    exchange_2      int,
    deleted_at      timestamp,
    PRIMARY key (id),
    CONSTRAINT products_category_fk FOREIGN KEY (category_id) REFERENCES product_category (id),
    CONSTRAINT products_unit1_fk FOREIGN KEY (unit_id_1) REFERENCES units (id),
    CONSTRAINT products_unit2_fk FOREIGN KEY (unit_id_2) REFERENCES units (id)
);
/*Partner*/
create table partner
(
    id           bigserial not null,
    name         varchar   not null,
    address      varchar,
    phone_number varchar,
    active       boolean,
    groups       varchar,
    wholesale    boolean,
    tag          text[],
    deleted_at   timestamp,
    PRIMARY key (id)
);
create table payment
(
    id         bigserial not null,
    content    varchar,
    created_at int8,
    partner_id int       not null,
    money      float,
    method     varchar,
    user_id    int,
    deleted_at timestamp,
    PRIMARY key (id)
);
create table receipt
(
    id         bigserial not null,
    content    varchar,
    created_at int8,
    partner_id int       not null,
    money      float,
    method     varchar,
    user_id    int,
    deleted_at timestamp,
    PRIMARY key (id)
);
create table import
(
    id         bigserial not null,
    content    varchar,
    created_at int8,
    partner_id int       not null,
    money      float,
    method     varchar,
    user_id    int,
    deleted_at timestamp,
    PRIMARY key (id)
);
create table export
(
    id          bigserial    not null,
    created_at  timestamptz  not null,
    partner_id  int          not null,
    content     varchar(255) not null,
    payments    float        not null,
    bill_number varchar(255) not null,
    created_by  int          not null,
    note        varchar(255),
    status      bool         not null,
    PRIMARY key (id),
    CONSTRAINT export_partner_fk FOREIGN KEY (partner_id) REFERENCES partner (id),
    CONSTRAINT export_user_fk FOREIGN KEY (created_by) REFERENCES users (id)
);
create table export_detail
(
    id               bigserial not null,
    bill_id          int       not null,
    product_id       int       not null,
    amount           int       not null,
    price            int       not null,
    origin_price     int,
    discount_percent float     not null,
    deleted_at       timestamptz,
    PRIMARY key (id),
    CONSTRAINT export_detail_fk FOREIGN KEY (bill_id) REFERENCES export (id),
    CONSTRAINT export_detail_product_fk FOREIGN KEY (product_id) REFERENCES products (id)
);
create table tag
(
    id         bigserial not null,
    name       varchar,
    deleted_at timestamp,
    PRIMARY key (id)
);