package errorcode

type Message struct {
	Code    int
	Message string
}

var Messages map[string]Message

const (
	UnknownError              = "unknown.error"
	InvaidPayload             = "invalid.payload"
	WrongEmailPassword        = "wrong.email.password"
	InvalidRefeshToken        = "invalid.refesh.token"
	InvalidAuthorizationToken = "invalid.authorization.token"
	PlatformTypeRequired      = "platform.type.required"
	PlatformTypeInvalid       = "platform.type.invalid"
	PermissonDenied           = "permisson.denied"
	EmailExisted              = "email.existed"
	FeedHadPosted             = "post.has.posted"
	MissingFile               = "missing.file"
)

func init() {
	Messages = make(map[string]Message)
	Messages[UnknownError] = Message{Code: 1000, Message: "Unknown Error"}
	Messages[InvaidPayload] = Message{Code: 1001, Message: "Invalid Payload"}
	Messages[WrongEmailPassword] = Message{Code: 1002, Message: "Wrong Email or Password"}
	Messages[InvalidRefeshToken] = Message{Code: 1003, Message: "Invalid Refesh Token"}
	Messages[InvalidAuthorizationToken] = Message{Code: 1004, Message: "Invalid Authorization Token"}
	Messages[PlatformTypeRequired] = Message{Code: 1005, Message: "Platform Type Is Required"}
	Messages[PlatformTypeInvalid] = Message{Code: 1006, Message: "Platform Type Is Invalid"}
	Messages[PlatformTypeInvalid] = Message{Code: 1007, Message: "Platform Type Is Invalid"}
	Messages[PermissonDenied] = Message{Code: 1008, Message: "Permisson Denied"}
	Messages[EmailExisted] = Message{Code: 1009, Message: "Email or Phone existed!'"}
	Messages[FeedHadPosted] = Message{Code: 1010, Message: "Feed had posted, can not edit"}
	Messages[MissingFile] = Message{Code: 1011, Message: "File is missing"}
}
