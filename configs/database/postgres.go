package database

import (
	"fmt"
	"github.com/jinzhu/gorm"
	"log"
)

type DBConfig struct {
	Host     string
	Port     string
	DbName   string
	User     string
	Password string
	SslMode  string
}
type Config struct {
	DB *DBConfig
}

func GetConfig() *Config {
	return &Config{
		DB: &DBConfig{
			Host:     "35.247.180.75",
			Port:     "5432",
			DbName:   "test",
			User:     "postgres",
			Password: "123123",
			SslMode:  "disable",
		},
	}
}

type SQL struct {
	DB *gorm.DB
}

func NewSQL() *SQL {
	config := GetConfig()
	URL := fmt.Sprintf("host=%v port=%v user=%v dbname=%v password=%v sslmode=%v",
		config.DB.Host,
		config.DB.Port,
		config.DB.User,
		config.DB.DbName,
		config.DB.Password,
		config.DB.SslMode)
	db, err := gorm.Open("postgres", URL)
	if err != nil {
		log.Fatal(err)
		return nil
	}
	db.LogMode(true)
	db.SingularTable(true)
	err = db.DB().Ping()
	if err != nil {
		fmt.Errorf("%s", err.Error())
		return nil
	}
	fmt.Println("Postgresql was connected!")
	//db.CreateTable(abc{})
	return &SQL{db}
}

type abc struct {
	gorm.Model
}
