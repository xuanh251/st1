package services

import (
	"fmt"
	"log"
	"net/smtp"
	"st1v2/configs/mail"
)

type Request struct {
	from    string
	to      []string
	subject string
	body    string
}

type MailContent struct {
	Password string
	Url      string
}

func NewRequest(to []string, subject string) *Request {
	return &Request{
		to:      to,
		subject: subject,
	}
}
func RenderTemplate(data MailContent, to string) []byte {
	return []byte(`<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta name="viewport" content="width=device-width"/>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <title>Subscribe To My Blog - M.Labouardy</title>
    <style type="text/css">
        body {
            margin: 0 auto;
            padding: 0;
            min-width: 100%;
            font-family: sans-serif;
        }

        table {
            margin: 50px 0 50px 0;
        }

        .header {
            height: 40px;
            text-align: center;
            text-transform: uppercase;
            font-size: 24px;
            font-weight: bold;
        }

        .content {
            height: 100px;
            font-size: 18px;
            line-height: 30px;
        }

        .subscribe {
            height: 70px;
            text-align: center;
        }

        .button {
            text-align: center;
            font-size: 18px;
            font-family: sans-serif;
            font-weight: bold;
            padding: 0 30px 0 30px;
        }

        .button a {
            color: #FFFFFF;
            text-decoration: none;
        }

        .buttonwrapper {
            margin: 0 auto;
        }

        .footer {
            text-transform: uppercase;
            text-align: center;
            height: 40px;
            font-size: 14px;
            font-style: italic;
        }

        .footer a {
            color: #000000;
            text-decoration: none;
            font-style: normal;
        }
    </style>
</head>
<body bgcolor="#009587">
<table bgcolor="#FFFFFF" width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr class="header">
        <td style="padding: 40px;">
            Xác thực đăng ký tài khoản phần mềm quản lý kho
        </td>
    </tr>
    <tr class="content">
        <td style="padding:10px;">
            <p>
                Xin chào <b>` + to + `</b>, <br/>
                Dưới đây là thông tin đăng nhập của bạn cho phần mềm quản lý kho:<br/>
                Email: <b>` + to + `</b><br/>
                Mật khẩu: <b>` + data.Password + `</b><br/>
                Vui lòng nhấn vào nút xác thực bên dưới để xác thực tài khoản của bạn trước khi đăng nhập vào phần mềm.
            </p>
        </td>
    </tr>
    <tr class="subscribe">
        <td style="padding: 20px 0 0 0;">
            <table bgcolor="#009587" border="0" cellspacing="0" cellpadding="0" class="buttonwrapper">
                <tr>
                    <td class="button" height="45">
                        <a href=` + data.Url + ` target="_blank">XÁC THỰC TÀI KHOẢN</a>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr class="footer">
        <td style="padding: 40px;">
            Team phát triển phần mềm <a href="#" target="_blank">Quản lý kho</a>
        </td>
    </tr>
</table>
</body>
</html>`)
}
func (r *Request) isSendMail(maildata MailContent) bool {
	config := mail.GetConfig()
	r.body = string(RenderTemplate(maildata, r.to[0]))
	body := "To: " + r.to[0] + "\r\nSubject: " + r.subject + "\r\n" + config.Mail.MIME + "\r\n" + r.body
	SMTP := fmt.Sprintf("%s:%d", config.Mail.SmtpHost, config.Mail.SmtpPort)
	if err := smtp.SendMail(SMTP, smtp.PlainAuth("", config.Mail.SenderId, config.Mail.SenderPassword, config.Mail.SmtpHost), config.Mail.SenderId, r.to, []byte(body)); err != nil {
		return false
	}
	return true
}

func SendEmail(Receive_mail, Password, ConfirmToken string, BaseUrl string) {
	subject := "Xác thực thông tin đăng ký"
	receiver := Receive_mail
	r := NewRequest([]string{receiver}, subject)
	if ok := r.isSendMail(MailContent{Password: Password, Url: BaseUrl + ConfirmToken}); ok {
		log.Printf("Email has been sent to %s\n", r.to)
	} else {
		log.Printf("Failed to send the email to %s\n", r.to)
	}
}
