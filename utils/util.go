package utils

import (
	"database/sql"
	"encoding/json"
	"errors"
	"fmt"
	"github.com/dgrijalva/jwt-go"
	"github.com/lib/pq"
	"golang.org/x/crypto/bcrypt"
	"math/rand"
	"net/http"
	"regexp"
	"st1v2/models"
	"strings"
	"time"
)

const (
	charset = "abcdefghijklmnopqrstuvwxyz" + "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"
)

var (
	JwtKeySecret_Login         = []byte("6i7npSm2yDhsHt4Ny3ZAgYwfltb5CBK6")
	JwtKeySecret_ConfirmEmail  = []byte("TxkD47H6myLw0FIMYSXE8MLJZdhDcg8Q")
	JwtKeySecret_ResetPassword = []byte("fCaIYfYf5oD7YHhtB3Ul8lqzFqiwa3Ua")
)

var seededRand = rand.New(
	rand.NewSource(time.Now().UnixNano()))

func StringWithCharset(length int, charset string) string {
	b := make([]byte, length)
	for i := range b {
		b[i] = charset[seededRand.Intn(len(charset))]
	}
	return string(b)
}

func RespondWithError(w http.ResponseWriter, code int, message interface{}) {
	RespondWithJSON(w, code, message, "error")
}

func RespondWithJSON(w http.ResponseWriter, code int, payload interface{}, notification ...interface{}) {
	json_resq := map[string]interface{}{
		"success": true,
		"data":    payload,
	}
	if len(notification) > 0 {
		if notification[0] == "error" {
			json_resq = map[string]interface{}{
				"success": false,
				"data":    payload,
			}
		}
	}
	response, _ := json.Marshal(json_resq)
	w.Header().Set("Content-Type", "application/json")
	//w.Header().Set("Content-Encoding", "gzip")
	w.WriteHeader(code)
	w.Write(response)
}
func GenerateJWT_Login(user models.User) (string, error) {
	user.Password = ""
	token := jwt.New(jwt.SigningMethodHS256)
	claims := token.Claims.(jwt.MapClaims)
	claims["authorized"] = true
	claims["info"] = user
	claims["exp"] = time.Now().Add(time.Minute * 60 * 24).Unix()
	return token.SignedString(JwtKeySecret_Login)

}

type Claims struct {
	Username string `json:"username"`
	jwt.StandardClaims
}

func GenerateJWT_ConfirmEmail(roleId int64, userId int64, userName string, baseUrl string) (string, error) {
	token := jwt.New(jwt.SigningMethodHS256)
	claims := token.Claims.(jwt.MapClaims)
	claims["userId"] = userId
	claims["roleId"] = roleId
	claims["exp"] = time.Now().Add(time.Hour * 24).Unix()
	claims["fEBaseUrl"] = baseUrl
	claims["userName"] = userName

	return token.SignedString(JwtKeySecret_ConfirmEmail)
}
func Bcrypt(password string) ([]byte, error) {
	return bcrypt.GenerateFromPassword([]byte(password), bcrypt.DefaultCost)
}
func IsPassword(hashedPassword, password string) error {
	return bcrypt.CompareHashAndPassword([]byte(hashedPassword), []byte(password))
}
func GeneratePassword() string {
	return StringWithCharset(6, charset)
}
func IsInArray(array []string, elem string) bool {
	for _, val := range array {
		if val == elem {
			return true
		}
	}
	return false
}
func CustomFeild(elem interface{}, opt []string) map[string]interface{} {
	m := map[string]interface{}{}
	jsonencode, _ := json.Marshal(&elem)
	json.Unmarshal(jsonencode, &m)
	for k, _ := range m {
		if !IsInArray(opt, k) {
			delete(m, k)
		}
	}
	return m
}

func HandlerErrorFromDb(err error) interface{} {
	myerr, ok := err.(*pq.Error)
	if !ok {
		fmt.Println(false)
		return err.Error()
	}
	if strings.Contains(myerr.Detail, "already exists") {
		re, _ := regexp.Compile(`\(.*?\)=\(.*?\)`)
		rawStr := re.Find([]byte(myerr.Detail))
		if rawStr != nil {
			return errors.New("Lỗi: " + string(rawStr) + " đã tồn tại!").Error()
		}
		return err.Error()
	}
	return err.Error()
}

func MakeResultReceiver(length int) []interface{} {
	result := make([]interface{}, 0, length)
	for i := 0; i < length; i++ {
		var current interface{}
		current = struct{}{}
		result = append(result, &current)
	}
	return result
}
func TransferData(rows *sql.Rows, toArray bool) (interface{}, error) {
	cols, err := rows.Columns()
	if err != nil {
		return nil, err
	}
	collen := len(cols)
	rs := make([]map[string]interface{}, 0)
	for rows.Next() {
		ra := MakeResultReceiver(collen)
		if err := rows.Scan(ra...); err != nil {
			return nil, err
		}
		value := make(map[string]interface{})
		for i := 0; i < collen; i++ {
			k := cols[i]
			v := ra[i]
			value[k] = v
		}
		rs = append(rs, value)
	}
	if toArray {
		return rs, nil
	}
	return rs[0], nil
}
