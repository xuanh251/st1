package middleware

import (
	"errors"
	"fmt"
	"github.com/dgrijalva/jwt-go"
	"net/http"
	"st1v2/utils"
	"strings"
)

func IsAuth(endpoint func(http.ResponseWriter, *http.Request), listRole ...string) http.HandlerFunc {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		authorizationHeader := r.Header.Get("Authorization")

		if authorizationHeader != "" {
			bearerToken := strings.Split(authorizationHeader, " ")
			if len(bearerToken) == 2 {
				token, err := jwt.Parse(bearerToken[1],
					func(token *jwt.Token) (interface{}, error) {
						if _, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
							return nil, fmt.Errorf("Unauthorized")
						}
						return utils.JwtKeySecret_Login, nil
					})
				if err != nil {
					utils.RespondWithError(w, http.StatusUnauthorized, err.Error())
					return
				}

				if len(listRole) > 0 {
					userRole := token.Claims.(jwt.MapClaims)["info"].(map[string]interface{})["role"].(map[string]interface{})["name"].(string)
					if !isInArray(userRole, listRole...) {
						utils.RespondWithError(w, http.StatusUnauthorized, errors.New("Role limited").Error())
						return
					}
				}
				if token.Valid {
					endpoint(w, r)
				}
			}
		} else {
			w.WriteHeader(http.StatusUnauthorized)
			w.Write([]byte("Unauthorized"))
		}
	})
}
func isInArray(item string, arr ...string) bool {
	for _, value := range arr {
		if value == item {
			return true
		}
	}
	return false
}
